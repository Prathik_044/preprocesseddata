import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Writer;
import java.io.FileWriter;

public class EquiSized {
	public static void main(String args[]) {
		if(args.length == 0) {
			System.out.println("Please enter file name as command line argument");
			return;
		}

		BufferedReader br = null;
		File infile = new File(args[0]);

		int[][] A = new int[100][50];
		int[] b = new int[100];
		int line = 0;

		try {
			String curr_line;
			br = new BufferedReader(new FileReader(infile));

			while((curr_line = br.readLine()) != null) {
				String data[] = curr_line.split(",");
				if(data.length != 1) {
					for(int i =0;i<50;i++) {
						A[line][i] = Integer.parseInt(data[i]);
					}
					line++;
				}
				
				else {
					curr_line = br.readLine();
					data = curr_line.split(",");
					for(int i=0;i<100;i++) {
						b[i] = Integer.parseInt(data[i]);
					}
				}		
			}
		}catch(IOException e) {
			e.printStackTrace();
		}

		/*
		* Test code to check if values are read correctly
		* for(int i=0;i<100;i++) {
		*	for(int j=0;j<50;j++) {
		*		System.out.print(A[i][j]+",");
		* 	}
		* 	System.out.println();
		* }
		* System.out.println();
		*
		* for(int i=0;i<100;i++) {
		* 	System.out.print(b[i]+",");
		* }
		* System.out.println();
		**/

		//Computation of Ce

		/* 
		* Integer Max valueand wrap around taken under consideration
		* to keep the multiplication by k out of the computation for now.
		* So the value written to file is actually Se i.e. Ce/k
		**/

		int Ce=Integer.MAX_VALUE;
		int count = 0;
		int curr_cap = 0;
		for(int i =0; i<100;i++) {
			count = 0;
			curr_cap = 0;
			if(b[i] != 0) {
				for(int j=0;j<50;j++) {
					if(A[i][j] == 1) {
						count++;
					}
						
				}
				curr_cap = b[i]/count;
				if(Ce>curr_cap) {
					Ce = curr_cap;
				}
			}
		}
		//System.out.println(Ce);
		try{
			File outfile = new File("CeValues.txt");
			FileWriter writer = new FileWriter(outfile,true);
			String data[] = args[0].split("_");
			writer.append(""+Ce+"  "+data[0]+"\n");
			//Commented code was to check order of execution of files
			//writer.append(""+Ce+"\n");
			writer.close();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
}