import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Writer;
import java.io.FileWriter;


public class SimpleDataProcess {
	public static void main(String args[]) {
		if(args.length == 0) {
			System.out.println("Please enter file name as command line argument and re-run.");
			return;
		}

		BufferedReader br = null;
		File infile = new File(args[0]);

		int[][] A = new int[100][50];
		int[] b = new int[100];
		int line = 0;

		try{

			String curr_line;
			br = new BufferedReader(new FileReader(infile));

			while((curr_line = br.readLine()) != null) {
				if(!curr_line.equals("\n")) {
					// if the line contains information about a node, we have to update the Array A and b
					String data[] = curr_line.split(",");
					if(data.length != 1) {
						int index = Integer.parseInt(data[1]);
						if(index>=100) {
							index = index -100;
						}
						int cap = Integer.parseInt(data[2]);
						A[index][line] = 1;

						if(b[index] != 0) {
							if(b[index] > cap) {
								b[index] = cap;
							}	
						}

						else {
							b[index] = cap;
						}
					}
					
					else {
						line++;
					}
				}
			}
		}catch(IOException e) {
			e.printStackTrace();
		}

		String name[] = args[0].split("_");
		String newfilename = name[0]+name[1]+"_Values.csv";
		try {

			FileWriter writer = new FileWriter(newfilename);

			for(int i = 0; i<100;i++) {
				for(int j = 0;j<50;j++) {
					if(j!=49) {
						writer.append(A[i][j]+",");
					}
					else {
						writer.append(A[i][j]+"\n");
					}

				}
			}
			writer.append("\n");

			
			for(int j = 0;j<100;j++) {
				if(j!=99) {
					writer.append(b[j]+",");
				}
				else {
					writer.append(b[j]+"\n");
				}
			}
			
			writer.close();
		}catch(IOException e) {
				e.printStackTrace();
		}
	}
}