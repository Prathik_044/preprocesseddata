import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Writer;
import java.io.FileWriter;
import java.util.ArrayList;

public class DataProcess {

	public static void main(String args[]) {
		if(args.length == 0) {
			System.out.println("Please enter file name as command line argument and re-run.");
			return;
		}

		BufferedReader br = null;
		File infile = new File(args[0]);
		ArrayList<ArrayList<MsgHost>> DelGraph = new ArrayList<ArrayList<MsgHost>>();
		ArrayList<MsgHost> flow = new ArrayList<MsgHost>();
		MsgHost	m;

		//Variable keeping track of Messages and nodes it passes through.
		int[][] A = new int[100][50];
		//Varialbe keeping track of free buffer per node.
		int[][] b = new int[100][50];
		//Variable to keep track of message number.
		int line = 0;

		try{

			String curr_line;
			br = new BufferedReader(new FileReader(infile));

			while((curr_line = br.readLine()) != null) {
				if(!curr_line.equals("\n")) {
					// if the line contains information about a node, we have to update the Array A and b
					String data[] = curr_line.split(",");
					if(data.length != 1) {
						//The address range is 2-101 Since first two addresses are stationary change
						int node = Integer.parseInt(data[1]);
						int capacity = Integer.parseInt(data[2]);
						double time = Double.parseDouble(data[3]);

						m = new MsgHost(node,capacity,time);
						flow.add(m);
						m = null;
					}

					/* Store the final delivery time, add the flow to the delivery graph if flow 
					 * is non-overlappting and start new flow.
					 **/
					else {
						m = new MsgHost(1,0,Double.parseDouble(data[0]));
						flow.add(m);
						if(DeliveryGraph.overlap(DelGraph,flow)) {
							DelGraph.add(flow);
							//add the node data to the arrays
							for(MsgHost n :flow) {
								A[n.id][line] = 1;
							//what to do id node is being used in a non-overlapping way? Lower value?
								b[n.id][line] = n.cap;
							}
						}
						flow = new ArrayList<MsgHost>();
					}
					 
					/*Logic would state that capacity evaluation can also include similar paths again
					* if the path is used again on non=overlapping time slots. Problem is that this cannot
					* be verified with current data.
					* The formula takes care of multiple flows, no need to add the
					* overlap checking, unless post completion flows is to be added.
					**/
				}

				else {
					line++;
				}
			}
		}catch(IOException e) {
			e.printStackTrace();
		}
		// Now we write A and B to file.
		String name[] = args[0].split("_");
		String newfilename = name[1]+"_Values.csv";
		try {

			FileWriter writer = new FileWriter(newfilename);

			for(int i = 0; i<100;i++) {
				for(int j = 0;j<50;j++) {
					if(j!=49) {
						writer.append(A[i][j]+",");
					}
					else {
						writer.append(A[i][j]+"\n");
					}

				}
			}
			writer.append("\n");

			for(int i = 0; i<100;i++) {
				for(int j = 0;j<50;j++) {
					if(j!=49) {
						writer.append(b[i][j]+",");
					}
					else {
						writer.append(b[i][j]+"\n");
					}

				}
			}
			
			writer.close();
		}catch(IOException e) {
				e.printStackTrace();
			}
	}
}


class MsgHost {
	int id;
	int cap;
	double time;

	public MsgHost(int id, int cap, double time) {
		this.id = id;
		this.cap = cap;
		this.time = time;
	}
}

class DeliveryGraph {

	/*Checking whether any previous flow is using one or more of the nodes being
	 * used by the flow under consideration. If yes then flow is to be discarded as resource
	 * has already been taken for capacity computation. Returns true if flow is non-overlapping.
	 **/
	static boolean overlap(ArrayList<ArrayList<MsgHost>> DelGraph, ArrayList<MsgHost> curr_flow) {
		for(ArrayList<MsgHost> flow : DelGraph) {
			for(MsgHost m1 : curr_flow) {
				int index = 0;
				for(MsgHost m2 : flow) {
					//checking if both flows have the same node
					if(m1.id == m2.id) {
						/** checking if the nodes have been used at the same instant.
						 *  logic is that the node shouldn't be used atleast until till the next 
						 *  node of the flow receives the message.
						 **/
						if( m1.time < ((flow.get(index+1)).time)) {
							return false;
						}
					}
					index++;
				}
			}
		}
		return true;
	}
}
