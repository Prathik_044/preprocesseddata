import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class TestReader {
	public static void main(String args[]) {
		if(args.length == 0) {
			System.out.println("Please enter file name as command line argument and re-run.");
			return;
		}

		BufferedReader br = null;
		File infile = new File(args[0]);
		try{

			String curr_line;
			br = new BufferedReader(new FileReader(infile));

			while((curr_line = br.readLine()) != null) {
				if(!curr_line.equals("\n")) {
					// if the line contains information about a node, we have to update the Array A and b
					String data[] = curr_line.split(",");
					System.out.println(data.length);
					if(data.length == 1) {
						if(data[0].length() == 1) {
						char c = data[0].charAt(0);
						System.out.println((int)c);
					 }
					}
				}
			}
		}catch(IOException e) {
			e.printStackTrace();
		}

	}
}